import logo from './logo.svg';
import './App.css';
import Botonera from './components/Botonera';

function App() {

	return (
		<div className="App">
			<header className="App-header">

				<Botonera name="Holis"></Botonera>

			</header>
		</div>
	);
}

export default App;
