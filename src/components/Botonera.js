import React from 'react';
import { Button, Grid } from '@mui/material';
/**
 * - Acá se importan las imágenes que se van a usar en los botones.
 * - El nombre que va después de "import",
 *   es el nombre de la variable donde se va a almacenar la imagen.
 * - El nombre que va después de "from",
 *   es el nombre del archivo correspondiente a la imagen.
 */
import img1 from './EO-5.jpg';
import img2 from './EO-5.jpg';
import img3 from './EO-5.jpg';
import img4 from './EO-5.jpg';
import img5 from './EO-5.jpg';
import img6 from './EO-5.jpg';
import img7 from './EO-5.jpg';
import img8 from './EO-5.jpg';
import img9 from './EO-5.jpg';

class Botonera extends React.Component {

	state = {
		boton1: 0,
		boton2: 0,
		boton3: 0,
		boton4: 0,
		boton5: 0,
		boton6: 0,
		boton7: 0,
		boton8: 0,
		boton9: 0,
	};

	componentDidMount() {
		window.addEventListener("keydown", this.handleKeyPress);
	}

	componentWillUnmount() {
		window.removeEventListener("keydown", this.handleKeyPress);
	}

	handleKeyPress = (ev) => {
		this.actualizarContador(ev.key)
	}

	handleClick = (ev, buttonId) => {
		this.actualizarContador(buttonId)
	}

	actualizarContador = (buttonId) => {
		switch (buttonId) {
			case "1":
				this.setState({
					boton1: this.state.boton1 + 1,
				});
				break;

			case "2":
				this.setState({
					boton2: this.state.boton2 + 1,
				});
				break;

			case "3":
				this.setState({
					boton3: this.state.boton3 + 1,
				});
				break;

			case "4":
				this.setState({
					boton4: this.state.boton4 + 1,
				});
				break;

			case "5":
				this.setState({
					boton5: this.state.boton5 + 1,
				});
				break;

			case "6":
				this.setState({
					boton6: this.state.boton6 + 1,
				});
				break;

			case "7":
				this.setState({
					boton7: this.state.boton7 + 1,
				});
				break;

			case "8":
				this.setState({
					boton8: this.state.boton8 + 1,
				});
				break;

			case "9":
				this.setState({
					boton9: this.state.boton9 + 1,
				});
				break;

			default:
				break;
		}

	}

	render() {
		let totalCount = this.state.boton1 + this.state.boton2 +
			this.state.boton3 + this.state.boton4 + this.state.boton5 +
			this.state.boton6 + this.state.boton7 + this.state.boton8 +
			this.state.boton9

		return <div>
			<Grid className='Grid-row' container spacing={2}>
				<Grid item xs={4}>
					<Button variant="contained"
						onClick={(ev) => this.handleClick(ev, "7")}>
						<img src={img7} alt='' />
						<span>{this.state.boton7}</span>
					</Button>
				</Grid>
				<Grid item xs={4}>
					<Button variant="contained"
						onClick={(ev) => this.handleClick(ev, "8")}>
						<img src={img8} alt='' />
						<span>{this.state.boton8}</span>
					</Button>
				</Grid>
				<Grid item xs={4}>
					<Button variant="contained"
						onClick={(ev) => this.handleClick(ev, "9")}>
						<img src={img9} alt='' />
						<span>{this.state.boton9}</span>
					</Button>
				</Grid>
			</Grid>
			<Grid className='Grid-row' container spacing={2}>
				<Grid item xs={4}>
					<Button variant="contained"
						onClick={(ev) => this.handleClick(ev, "4")}>
						<img src={img4} alt='' />
						<span>{this.state.boton4}</span>
					</Button>
				</Grid>
				<Grid item xs={4}>
					<Button variant="contained"
						onClick={(ev) => this.handleClick(ev, "5")}>
						<img src={img5} alt='' />
						<span>{this.state.boton5}</span>
					</Button>
				</Grid>
				<Grid item xs={4}>
					<Button variant="contained"
						onClick={(ev) => this.handleClick(ev, "6")}>
						<img src={img6} alt='' />
						<span>{this.state.boton6}</span>
					</Button>
				</Grid>
			</Grid>
			<Grid className='Grid-row' container spacing={2}>
				<Grid item xs={4}>
					<Button variant="contained"
						onClick={(ev) => this.handleClick(ev, "1")}>
						<img src={img1} alt='' />
						<span>{this.state.boton1}</span>
					</Button>
				</Grid>
				<Grid item xs={4}>
					<Button variant="contained"
						onClick={(ev) => this.handleClick(ev, "2")}>
						<img src={img2} alt='' />
						<span>{this.state.boton2}</span>
					</Button>
				</Grid>
				<Grid item xs={4}>
					<Button variant="contained"
						onClick={(ev) => this.handleClick(ev, "3")}>
						<img src={img3} alt='' />
						<span>{this.state.boton3}</span>
					</Button>
				</Grid>
			</Grid>

			<h1>Total: {totalCount}</h1>
		</div>;
	}
}

export default Botonera;